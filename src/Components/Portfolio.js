import React, { Component } from 'react';
import {Animated} from "react-animated-css";
import ScrollAnimation from 'react-animate-on-scroll';

class Portfolio extends Component {
  render() {

    if(this.props.data){
      var projects = this.props.data.projects.map(function(projects){
        var projectImage = 'images/portfolio/'+projects.image;
        return <div key={projects.title} className="columns portfolio-item">
          <ScrollAnimation animateIn="rotateIn" duration="5">
            
           <div className="item-wrap">
            <a href={projects.url} title={projects.title}>
            <Animated animationIn="bounceInRight" animationOut="bounce" isVisible={true}>
              <img alt={projects.title} src={projectImage} />
              </Animated>
                <div className="overlay">
                  <div className="portfolio-item-meta">
                    <h5>{projects.title}</h5>
                     <p>{projects.category}</p>
                  </div>
                </div>
              <div className="link-icon"><i className="fa fa-link"></i></div>
            </a>
          </div>
            </ScrollAnimation>
        </div>
      })
    }

    return (
      <section id="portfolio">

      <div className="row">

         <div className="twelve columns collapsed">

            <h1>Check Out Some of My Works.</h1>
              <div id="portfolio-wrapper" className="bgrid-quarters s-bgrid-thirds cf">
                  {projects}
              </div>
          </div>
      </div>
   </section>
    );
  }
}

export default Portfolio;
